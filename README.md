# React-NodeJS Admin Asset Manager

first time let clone repo
`git clone https://gitlab.com/DungBuiDeveloper/asset-manager-system.git`

# FEATURE

    ## BACK-END
    - REST API NODE JS
    - Json Webtoken
    - MongoDB
    - Social login

    ## FRONT-END
    - Refesh token automatic
    - React Hook
    - Using Redux with React Hooks
    - React Redux Saga
    - React Redux Form
    - i18n
    - Material UI

## prepare env

- node: v10.15.3
- mongod 4.4.0

## Front-End

### Install

- `cd asset-manager-system/frontend`
- `npm i`
- `npm start`
- goto http://localhost:3000

### Axios Config API

- Change Base API URL : your_project_dir/frontend/app/utils/axios/constants.js
- Axios Option in each your saga

##### GET REQUEST

```sh
const data = yield call(axiosRequest, {
    url: requestURL,
    method: 'get',
    auth: true,
    params: {
    limit: 10,
    filter: 'search_text',
    fields: 'name',
    page: 1,
    sort: 'name',
    order: '-1',
    },
});

```

##### POST REQUEST

```sh
const data = yield call(axiosRequest, {
    url: requestURL,
    method: 'post',
    auth: true,
    data:{},
});

```

## Back-End

- cd myproject/backend
- npm install
- npm update

### Setting up environments (development or production)

1. In the root this repository you will find a file named .env.example

2. Create a new file by copying and pasting the file and then renaming it to just .env
3. The file .env is already ignored, so you never commit your credentials.
4. Change the values of the file to your environment (development or production)
5. Upload the .env to your environment server(development or production)
6. If you use the postman collection to try the endpoints, change value of the variable server on your environment to the url of your server, for development mode use http://localhost:4000

### Database cleaning and seeding samples

`npm run fresh`
`npm run clean`
`npm run seed`

- fresh cleans and then seeds the database with dynamic data.
- clean cleans the database.
- seed seeds the database with dynamic data.

## Login credentials
- email: admin@admin.com
- password: 12345

`npm start`

- You will know server is running by checking the output of the command npm run dev

`

---

- Starting Server
- Port: 4000
- NODE_ENV: development
- Database: MongoDB
- DB Connection: OK

---

`

### Postman API example collection

- post_man_asset_manager.json import this file into postman collection for test api

## DEPLOY

- we will deploy with nodedock https://nodedock.io/
- document for deploy comming soon ...
