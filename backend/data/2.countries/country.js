const faker = require('faker')

const json = [
  {
    name: 'Afghanistan',
    code: 'AF',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Åland Islands',
    code: 'AX',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Albania',
    code: 'AL',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Algeria',
    code: 'DZ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'American Samoa',
    code: 'AS',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'AndorrA',
    code: 'AD',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Angola',
    code: 'AO',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Anguilla',
    code: 'AI',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Antarctica',
    code: 'AQ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Antigua and Barbuda',
    code: 'AG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Argentina',
    code: 'AR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Armenia',
    code: 'AM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Aruba',
    code: 'AW',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Australia',
    code: 'AU',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Austria',
    code: 'AT',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Azerbaijan',
    code: 'AZ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Bahamas',
    code: 'BS',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Bahrain',
    code: 'BH',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Bangladesh',
    code: 'BD',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Barbados',
    code: 'BB',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Belarus',
    code: 'BY',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Belgium',
    code: 'BE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Belize',
    code: 'BZ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Benin',
    code: 'BJ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Bermuda',
    code: 'BM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Bhutan',
    code: 'BT',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Bolivia',
    code: 'BO',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Bosnia and Herzegovina',
    code: 'BA',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Botswana',
    code: 'BW',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Bouvet Island',
    code: 'BV',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Brazil',
    code: 'BR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'British Indian Ocean Territory',
    code: 'IO',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Brunei Darussalam',
    code: 'BN',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Bulgaria',
    code: 'BG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Burkina Faso',
    code: 'BF',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Burundi',
    code: 'BI',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Cambodia',
    code: 'KH',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Cameroon',
    code: 'CM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Canada',
    code: 'CA',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Cape Verde',
    code: 'CV',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Cayman Islands',
    code: 'KY',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Central African Republic',
    code: 'CF',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Chad',
    code: 'TD',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Chile',
    code: 'CL',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'China',
    code: 'CN',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Christmas Island',
    code: 'CX',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Cocos (Keeling) Islands',
    code: 'CC',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Colombia',
    code: 'CO',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Comoros',
    code: 'KM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Congo',
    code: 'CG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Congo, The Democratic Republic of the',
    code: 'CD',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Cook Islands',
    code: 'CK',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Costa Rica',
    code: 'CR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: `Cote D'Ivoire`,
    code: 'CI',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Croatia',
    code: 'HR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Cuba',
    code: 'CU',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Cyprus',
    code: 'CY',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Czech Republic',
    code: 'CZ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Denmark',
    code: 'DK',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Djibouti',
    code: 'DJ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Dominica',
    code: 'DM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Dominican Republic',
    code: 'DO',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Ecuador',
    code: 'EC',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Egypt',
    code: 'EG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'El Salvador',
    code: 'SV',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Equatorial Guinea',
    code: 'GQ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Eritrea',
    code: 'ER',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Estonia',
    code: 'EE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Ethiopia',
    code: 'ET',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Falkland Islands (Malvinas)',
    code: 'FK',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Faroe Islands',
    code: 'FO',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Fiji',
    code: 'FJ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Finland',
    code: 'FI',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'France',
    code: 'FR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'French Guiana',
    code: 'GF',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'French Polynesia',
    code: 'PF',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'French Southern Territories',
    code: 'TF',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Gabon',
    code: 'GA',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Gambia',
    code: 'GM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Georgia',
    code: 'GE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Germany',
    code: 'DE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Ghana',
    code: 'GH',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Gibraltar',
    code: 'GI',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Greece',
    code: 'GR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Greenland',
    code: 'GL',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Grenada',
    code: 'GD',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Guadeloupe',
    code: 'GP',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Guam',
    code: 'GU',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Guatemala',
    code: 'GT',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Guernsey',
    code: 'GG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Guinea',
    code: 'GN',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Guinea-Bissau',
    code: 'GW',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Guyana',
    code: 'GY',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Haiti',
    code: 'HT',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Heard Island and Mcdonald Islands',
    code: 'HM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Holy See (Vatican City State)',
    code: 'VA',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Honduras',
    code: 'HN',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Hong Kong',
    code: 'HK',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Hungary',
    code: 'HU',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Iceland',
    code: 'IS',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'India',
    code: 'IN',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Indonesia',
    code: 'ID',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Iran, Islamic Republic Of',
    code: 'IR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Iraq',
    code: 'IQ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Ireland',
    code: 'IE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Isle of Man',
    code: 'IM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Israel',
    code: 'IL',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Italy',
    code: 'IT',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Jamaica',
    code: 'JM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Japan',
    code: 'JP',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Jersey',
    code: 'JE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Jordan',
    code: 'JO',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Kazakhstan',
    code: 'KZ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Kenya',
    code: 'KE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Kiribati',
    code: 'KI',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: `Korea, Democratic People'S Republic of`,
    code: 'KP',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Korea, Republic of',
    code: 'KR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Kuwait',
    code: 'KW',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Kyrgyzstan',
    code: 'KG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: `Lao People'S Democratic Republic`,
    code: 'LA',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Latvia',
    code: 'LV',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Lebanon',
    code: 'LB',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Lesotho',
    code: 'LS',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Liberia',
    code: 'LR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Libyan Arab Jamahiriya',
    code: 'LY',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Liechtenstein',
    code: 'LI',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Lithuania',
    code: 'LT',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Luxembourg',
    code: 'LU',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Macao',
    code: 'MO',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Macedonia, The Former Yugoslav Republic of',
    code: 'MK',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Madagascar',
    code: 'MG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Malawi',
    code: 'MW',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Malaysia',
    code: 'MY',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Maldives',
    code: 'MV',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Mali',
    code: 'ML',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Malta',
    code: 'MT',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Marshall Islands',
    code: 'MH',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Martinique',
    code: 'MQ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Mauritania',
    code: 'MR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Mauritius',
    code: 'MU',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Mayotte',
    code: 'YT',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Mexico',
    code: 'MX',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Micronesia, Federated States of',
    code: 'FM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Moldova, Republic of',
    code: 'MD',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Monaco',
    code: 'MC',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Mongolia',
    code: 'MN',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Montserrat',
    code: 'MS',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Morocco',
    code: 'MA',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Mozambique',
    code: 'MZ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Myanmar',
    code: 'MM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Namibia',
    code: 'NA',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Nauru',
    code: 'NR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Nepal',
    code: 'NP',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Netherlands',
    code: 'NL',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Netherlands Antilles',
    code: 'AN',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'New Caledonia',
    code: 'NC',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'New Zealand',
    code: 'NZ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Nicaragua',
    code: 'NI',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Niger',
    code: 'NE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Nigeria',
    code: 'NG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Niue',
    code: 'NU',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Norfolk Island',
    code: 'NF',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Northern Mariana Islands',
    code: 'MP',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Norway',
    code: 'NO',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Oman',
    code: 'OM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Pakistan',
    code: 'PK',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Palau',
    code: 'PW',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Palestinian Territory, Occupied',
    code: 'PS',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Panama',
    code: 'PA',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Papua New Guinea',
    code: 'PG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Paraguay',
    code: 'PY',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Peru',
    code: 'PE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Philippines',
    code: 'PH',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Pitcairn',
    code: 'PN',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Poland',
    code: 'PL',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Portugal',
    code: 'PT',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Puerto Rico',
    code: 'PR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Qatar',
    code: 'QA',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Reunion',
    code: 'RE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Romania',
    code: 'RO',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Russian Federation',
    code: 'RU',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'RWANDA',
    code: 'RW',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Saint Helena',
    code: 'SH',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Saint Kitts and Nevis',
    code: 'KN',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Saint Lucia',
    code: 'LC',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Saint Pierre and Miquelon',
    code: 'PM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Saint Vincent and the Grenadines',
    code: 'VC',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Samoa',
    code: 'WS',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'San Marino',
    code: 'SM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Sao Tome and Principe',
    code: 'ST',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Saudi Arabia',
    code: 'SA',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Senegal',
    code: 'SN',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Serbia and Montenegro',
    code: 'CS',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Seychelles',
    code: 'SC',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Sierra Leone',
    code: 'SL',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Singapore',
    code: 'SG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Slovakia',
    code: 'SK',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Slovenia',
    code: 'SI',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Solomon Islands',
    code: 'SB',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Somalia',
    code: 'SO',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'South Africa',
    code: 'ZA',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'South Georgia and the South Sandwich Islands',
    code: 'GS',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Spain',
    code: 'ES',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Sri Lanka',
    code: 'LK',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Sudan',
    code: 'SD',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Suriname',
    code: 'SR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Svalbard and Jan Mayen',
    code: 'SJ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Swaziland',
    code: 'SZ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Sweden',
    code: 'SE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Switzerland',
    code: 'CH',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Syrian Arab Republic',
    code: 'SY',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Taiwan, Province of China',
    code: 'TW',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Tajikistan',
    code: 'TJ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Tanzania, United Republic of',
    code: 'TZ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Thailand',
    code: 'TH',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Timor-Leste',
    code: 'TL',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Togo',
    code: 'TG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Tokelau',
    code: 'TK',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Tonga',
    code: 'TO',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Trinidad and Tobago',
    code: 'TT',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Tunisia',
    code: 'TN',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Turkey',
    code: 'TR',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Turkmenistan',
    code: 'TM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Turks and Caicos Islands',
    code: 'TC',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Tuvalu',
    code: 'TV',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Uganda',
    code: 'UG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Ukraine',
    code: 'UA',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'United Arab Emirates',
    code: 'AE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'United Kingdom',
    code: 'GB',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'United States',
    code: 'US',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'United States Minor Outlying Islands',
    code: 'UM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Uruguay',
    code: 'UY',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Uzbekistan',
    code: 'UZ',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Vanuatu',
    code: 'VU',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Venezuela',
    code: 'VE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Viet Nam',
    code: 'VN',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Virgin Islands, British',
    code: 'VG',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Virgin Islands, U.S.',
    code: 'VI',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Wallis and Futuna',
    code: 'WF',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Western Sahara',
    code: 'EH',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Yemen',
    code: 'YE',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Zambia',
    code: 'ZM',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  },
  {
    name: 'Zimbabwe',
    code: 'ZW',
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  }
]

module.exports = json
