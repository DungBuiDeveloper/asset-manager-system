const faker = require('faker')

const json = []
for (let index = 0; index < 200; index++) {
  json[index] = {
    name: faker.name.title(),
    url: faker.internet.url(),
    suportUrl: faker.internet.url(),
    suportPhone: faker.phone.phoneNumber(),
    suportEmail: faker.internet.email(),
    thumb: faker.image.imageUrl(),
    createdAt: faker.date.past(),
    updatedAt: faker.date.recent()
  }
}

module.exports = json
