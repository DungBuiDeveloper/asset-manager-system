const model = require('../models/country')
const { matchedData } = require('express-validator')
const utils = require('../middleware/utils')
const db = require('../middleware/db')
const formatFlag = (code) => {
  return `https://www.countryflags.io/${code}/flat/64.png`
}
/**
 * Get items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getItems = async (req, res) => {
  try {
    const query = await db.checkQueryString(req.query)

    const countries = await db.getItems(req, model, query)
    countries.docs.map((item) => {
      item.flag = formatFlag(item.code)
    })
    res.status(200).json(countries)
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Gets all items from database
 */
const getAllItemsFromDB = () => {
  return new Promise((resolve, reject) => {
    model.find(
      {},
      '-updatedAt -createdAt ',
      {
        sort: {
          name: 1
        }
      },
      (err, items) => {
        if (err) {
          reject(utils.buildErrObject(422, err.message))
        }
        resolve(items)
      }
    )
  })
}

/********************
 * Public functions *
 ********************/

/**
 * Get all items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getAllItems = async (req, res) => {
  try {
    const countries = await getAllItemsFromDB()
    countries.map((item) => {
      item.flag = formatFlag(item.code)
    })

    res.status(200).json({ countries })
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Get item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getItem = async (req, res) => {
  try {
    req = matchedData(req)
    const id = await utils.isIDGood(req.id)
    const country = await db.getItem(id, model)
    country.flag = formatFlag(country.code)
    res.status(200).json(country)
  } catch (error) {
    utils.handleError(res, error)
  }
}
