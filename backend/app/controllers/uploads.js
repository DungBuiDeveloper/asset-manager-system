// const { matchedData } = require('express-validator')
const utils = require('../middleware/utils')
// const db = require('../middleware/db')

/**
 * Get items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.upload = async (req, res) => {
  try {
    res.status(200).json({
      status: true,
      url: `${process.env.UPLOAD_FOLDER}${req.file.filename}`
    })
  } catch (error) {
    utils.handleError(res, error)
  }
}
