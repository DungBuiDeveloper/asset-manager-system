const { validationResult } = require('../middleware/utils')
const { check } = require('express-validator')
// var validator = require('validator')
/**
 * Validates create new item request
 */
exports.upload = [
  check('file')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY')
    .trim(),

  (req, res, next) => {
    validationResult(req, res, next)
  }
]
