const controller = require('../controllers/uploads')
// const validate = require('../controllers/upload.validate')
// const AuthController = require('../controllers/auth')
const router = require('express').Router()
require('../../config/passport')
// const passport = require('passport')
// const requireAuth = passport.authenticate('jwt', {
//   session: false
// })
const path = require('path')
const trimRequest = require('trim-request')

const multer = require('multer')

const storage = multer.diskStorage({
  destination(req, file, callback) {
    callback(null, process.env.UPLOAD_FOLDER)
  },
  filename(req, file, callback) {
    callback(
      null,
      `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`
    )
  }
})

const uploadImage = multer({
  storage,
  fileFilter(req, file, callback) {
    const ext = path.extname(file.originalname)
    if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
      return callback(new Error('Only images are allowed'))
    }

    callback(null, true)
    return true
  }
}).single('file')

/*
 * Create new item route
 */
router.post(
  '/',
  // requireAuth,
  // AuthController.roleAuthorization(['admin', 'user']),
  trimRequest.all,
  uploadImage,
  //   validate.upload,
  controller.upload
)

// /*
//  * Get item route
//  */
// router.get(
//   '/:id',
//   requireAuth,
//   AuthController.roleAuthorization(['admin']),
//   trimRequest.all,
//   validate.getItem,
//   controller.getItem
// )

// /*
//  * Update item route
//  */
// router.patch(
//   '/:id',
//   requireAuth,
//   AuthController.roleAuthorization(['admin']),
//   trimRequest.all,
//   validate.updateItem,
//   controller.updateItem
// )

// /*
//  * Delete item route
//  */
// router.delete(
//   '/:id',
//   requireAuth,
//   AuthController.roleAuthorization(['admin']),
//   trimRequest.all,
//   validate.deleteItem,
//   controller.deleteItem
// )

module.exports = router
