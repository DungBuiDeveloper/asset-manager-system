const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const DepreciationSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    month: {
      type: Number,
      required: true
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
DepreciationSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('depreciation', DepreciationSchema)
