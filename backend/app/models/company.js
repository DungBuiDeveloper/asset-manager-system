const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const CompanySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    thumbnail: {
      type: String
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
CompanySchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Company', CompanySchema)
