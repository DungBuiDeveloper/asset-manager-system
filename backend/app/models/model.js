const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const modelAssetSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    manufacturerId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'manufacturers',
      required: true
    },
    categoryId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'categories',
      required: true
    },
    modelNo: {
      type: String,
      required: true,
      unique: true
    },
    depreciationId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'depreciation'
    },
    eol: {
      type: Number,
      min: 1,
      max: 12
    },
    note: {
      type: String
    },
    thumb: {
      type: String
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
modelAssetSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('models', modelAssetSchema)
