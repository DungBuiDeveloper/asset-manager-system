const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const ComponentSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    categoryId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'categories'
    },
    quantity: {
      type: Number,
      required: true
    },
    minQty: {
      type: Number,
      required: true
    },
    serial: {
      type: String,
      required: true
    },
    companyId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Company'
    },
    localionId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Location'
    },
    orderNumber: {
      type: Number,
      required: true
    },
    purchaseDate: {
      type: Date,
      required: true
    },
    purchaseCost: {
      type: Number,
      required: true
    },
    thumbnail: {
      type: String
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
ComponentSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Component', ComponentSchema)
