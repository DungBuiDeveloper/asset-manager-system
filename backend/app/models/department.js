const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const DepartmentSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    companyId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Company'
    },
    managet: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'User'
    },
    locationId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Location'
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
DepartmentSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Department', DepartmentSchema)
