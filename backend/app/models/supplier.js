const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const SupplierSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    address: {
      type: String,
      required: true
    },
    city: {
      type: String,
      required: true
    },
    state: {
      type: String,
      required: true
    },
    countryId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Country'
    },
    zipcode: {
      type: String,
      required: true
    },
    contactName: {
      type: String,
      required: true
    },
    phone: {
      type: String,
      required: true
    },
    fax: {
      type: String
    },
    email: {
      type: String,
      required: true
    },
    url: {
      type: String,
      required: true
    },
    note: {
      type: String
    },
    thumbnail: {
      type: String
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
SupplierSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Supplier', SupplierSchema)
