const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const LocationSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    parent: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Location'
    },
    manager: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'User'
    },
    currency: {
      type: String,
      required: true
    },
    address: {
      type: String,
      required: true
    },
    city: {
      type: String,
      required: true
    },
    state: {
      type: String,
      required: true
    },
    zipcode: {
      type: String,
      required: true
    },
    thumbnail: {
      type: String
    },
    countryId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Country'
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
LocationSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Location', LocationSchema)
