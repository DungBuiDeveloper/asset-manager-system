const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const ConsumableSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    categoryId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'categories'
    },
    quantity: {
      type: Number,
      required: true
    },
    minQty: {
      type: Number,
      required: true
    },
    serial: {
      type: String,
      required: true
    },
    companyId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Company'
    },
    localionid: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Location'
    },
    manufacturerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'manufacturers'
    },
    orderNumber: {
      type: Number,
      required: true
    },
    purchaseDate: {
      type: Date,
      required: true
    },
    purchaseCost: {
      type: Number,
      required: true
    },
    modelNo: {
      type: String,
      required: true
    },
    itemNo: {
      type: String,
      required: true
    },
    thumbnail: {
      type: String
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
ConsumableSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Consumable', ConsumableSchema)
