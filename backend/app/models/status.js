const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const StatusSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    statusType: {
      type: String,
      required: true
    },
    colorStatus: {
      type: String,
      required: true
    },
    notes: {
      type: String
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
StatusSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Status', StatusSchema)
