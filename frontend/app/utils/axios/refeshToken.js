import moment from 'moment';
import axios from 'axios';
import { API_ROOT } from './constants';
import generateTokenNeedRefresh from '../generateTokenNeedRefresh';

function millisToMinutes(millis) {
  var minutes = Math.floor(millis / 60000);
  return minutes;
}

function requestGetToken() {
  var config = {
    method: 'get',
    url: `${API_ROOT}/token`,
    headers: {
      Authorization: 'Bearer ' + localStorage.getItem('token'),
    },
  };

  axios(config)
    .then(function(response) {
      localStorage.setItem('token', response.data.token);
      generateTokenNeedRefresh();
    })
    .catch(function(error) {
      alert('BAD TOKEN ');

      localStorage.removeItem('token');
      localStorage.removeItem('tokenExpiration');
      window.location.href = '/login';
    });
}

const refeshToken = () => {
  let now = moment(new Date());
  let tokenExpiration = moment(localStorage.getItem('tokenExpiration'));

  var durationMinute = millisToMinutes(
    moment.duration(tokenExpiration.diff(now)).asMilliseconds(),
  );

  if (durationMinute < 10) {
    requestGetToken();
  }
};

export default refeshToken;
