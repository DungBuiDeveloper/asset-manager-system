import axios from 'axios';
import { API_ROOT } from './constants';
import refeshToken from './refeshToken';
const axiosService = axios.create({
  baseURL: `${API_ROOT}/`,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
  },
});

// /*
//   Axios Barie
//  */

export default axiosService;

axiosService.interceptors.response.use(
  response => {
    if (response.config.url !== `${API_ROOT}/token`) {
      refeshToken();
    }

    return response;
  },
  error => {
    // Do something with response error
    return Promise.reject(error);
  },
);
