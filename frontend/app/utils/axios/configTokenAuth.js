const configTokenAuth = () => {
  let configReturn = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token'),
  };
  return configReturn;
};

export default configTokenAuth;
