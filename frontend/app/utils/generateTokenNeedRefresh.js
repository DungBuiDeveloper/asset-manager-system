import moment from 'moment';
const generateTokenNeedRefresh = () => {
  const MINUTES_TO_CHECK_FOR_TOKEN_REFRESH = 1440;
  let tokenExpiration = moment(new Date())
    .add(MINUTES_TO_CHECK_FOR_TOKEN_REFRESH, 'minutes')
    .toISOString();

  window.localStorage.setItem('tokenExpiration', tokenExpiration);
};

export default generateTokenNeedRefresh;
