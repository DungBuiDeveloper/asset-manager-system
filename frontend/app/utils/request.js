import axiosService from './axios/axiosConfig';

import configTokenAuth from './axios/configTokenAuth';
/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  if (response.status === 204 || response.status === 205) {
    return null;
  }

  return response.data;
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  // throw error;
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */

export function axiosRequest(options) {
  if (options.auth == true) {
    options.headers = configTokenAuth();
  }
  // options.baseURL = API_ROOT;
  delete options.auth;

  return axiosService
    .request(options)
    .then(checkStatus)
    .then(parseJSON);
}
