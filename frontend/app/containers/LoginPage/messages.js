/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'LoginPage';

export default defineMessages({
  USER_DOES_NOT_EXIST: {
    id: `${scope}.USER_DOES_NOT_EXIST`,
    defaultMessage: 'User does not exist',
  },
});
