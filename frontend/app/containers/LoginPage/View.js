import React from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';

import {
  Box,
  Button,
  Container,
  Grid,
  Link,
  Typography,
} from '@material-ui/core';
import FacebookIcon from '@material-ui/icons/Facebook';
import GoogleIcon from './Google';
import { Field, reduxForm } from 'redux-form';
import renderFieldInput from 'components/reduxForm/input';
import validate from './validate';

const LoginView = ({ handleSubmit, pristine, submitting, submitForm }) => {
  return (
    <div className="center_auth" id="login">
      <Box
        display="flex"
        flexDirection="column"
        height="100%"
        justifyContent="center"
      >
        <Container maxWidth="sm">
          <form onSubmit={handleSubmit(data => submitForm(data))}>
            <Box mb={3}>
              <Typography color="textPrimary" variant="h2">
                Sign in
              </Typography>
              <Typography color="textSecondary" gutterBottom variant="body2">
                Sign in on the internal platform
              </Typography>
            </Box>
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <Button
                  color="primary"
                  fullWidth
                  startIcon={<FacebookIcon />}
                  size="large"
                  variant="contained"
                >
                  Login with Facebook
                </Button>
              </Grid>
              <Grid item xs={12} md={6}>
                <Button
                  fullWidth
                  startIcon={<GoogleIcon />}
                  size="large"
                  variant="contained"
                >
                  Login with Google
                </Button>
              </Grid>
            </Grid>
            <Box mt={3} mb={1}>
              <Typography align="center" color="textSecondary" variant="body1">
                or login with email address
              </Typography>
            </Box>

            <Field
              label={'Email Address'}
              name="email"
              component={renderFieldInput}
              type="email"
              placeholder="Email Address"
              nameError="email"
            />
            {/* email */}

            <Field
              label={'Password'}
              name="password"
              component={renderFieldInput}
              type="password"
              placeholder="Password"
              nameError="password"
            />
            {/* Password */}

            <Box my={2}>
              <Button
                disabled={pristine || submitting}
                color="primary"
                fullWidth
                size="large"
                type="submit"
                variant="contained"
              >
                Sign in now
              </Button>
            </Box>
            <Typography color="textSecondary" variant="body1">
              Don&apos;t have an account?{' '}
              <Link component={RouterLink} to="/register" variant="h6">
                Sign up
              </Link>
            </Typography>
          </form>
        </Container>
      </Box>
    </div>
  );
};

export default reduxForm({
  form: 'loginForm',
  validate,
})(LoginView);
