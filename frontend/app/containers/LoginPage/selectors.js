/**
 * Blogpage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectLogin = state => state.login || initialState;

const makeSelectLoading = () =>
  createSelector(
    selectLogin,
    state => state.loading,
  );
const makeSelectError = () =>
  createSelector(
    selectLogin,
    state => state.error,
  );

export { selectLogin, makeSelectLoading, makeSelectError };
