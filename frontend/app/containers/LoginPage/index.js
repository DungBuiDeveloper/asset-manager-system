/*
 * LoginPage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import View from './View';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';

import { loginStart } from './actions';
import saga from './saga';
import reducer from './reducer';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { makeSelectError, makeSelectLoading } from './selectors';
import { createStructuredSelector } from 'reselect';
import LoadingIndicator from 'components/LoadingIndicator';
import Alert from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
const key = 'login';

export function LoginPage({ loading, error, history, loginStart }) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  function submitForm(data) {
    loginStart(data, history);
  }

  function renderAlert(error) {
    if (typeof error == 'string') {
      return (
        <Alert
          error={true}
          message={<FormattedMessage {...messages[error]} />}
        />
      );
    } else {
      return (
        <div>
          {error.map((item, i) => {
            return (
              <Alert
                key={i}
                error={true}
                message={<FormattedMessage {...messages[item.msg]} />}
              />
            );
          })}
        </div>
      );
    }
  }
  return (
    <article>
      <Helmet>
        <title>Login Page</title>
        <meta
          name="Login Page"
          content="A React.js Boilerplate application LoginPage"
        />
      </Helmet>

      {error != null ? renderAlert(error) : null}

      {loading ? <LoadingIndicator /> : null}
      <View submitForm={submitForm} />
    </article>
  );
}

LoginPage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
  loginStart: PropTypes.func,
  history: PropTypes.object,
};
const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    loginStart: (data, history) => dispatch(loginStart(data, history)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default withRouter(
  compose(
    withConnect,
    memo,
  )(LoginPage),
);
