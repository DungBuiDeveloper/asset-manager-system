/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const LOADING = 'login/LOADING';
export const LOADING_REMOVE = 'login/LOADING_REMOVE';
export const ERROR = 'login/ERROR';
export const START_LOGIN = 'login/START_LOGIN';
export const SUCCESS_LOGIN = 'login/SUCCESS_LOGIN';
