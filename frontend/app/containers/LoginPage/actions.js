import {
  LOADING,
  ERROR,
  LOADING_REMOVE,
  START_LOGIN,
  SUCCESS_LOGIN,
} from './constants';

export function loading() {
  return {
    type: LOADING,
  };
}

export function error(error) {
  let eMessage = error.errors.msg;
  return {
    type: ERROR,
    error: eMessage,
  };
}

export function removeLoading() {
  return {
    type: LOADING_REMOVE,
  };
}

export function loginStart(data, history) {
  return {
    type: START_LOGIN,
    data,
    history,
  };
}

export function loginSuccess(data) {
  console.log(data);
  return {
    type: SUCCESS_LOGIN,
    data: data.res,
  };
}
