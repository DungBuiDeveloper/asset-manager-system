import { call, put, takeLatest } from 'redux-saga/effects';

import { START_LOGIN } from './constants';
import { axiosRequest } from 'utils/request';
import { error, loginSuccess } from './actions';
import { API_ROOT } from 'containers/App/constants';
import generateTokenNeedRefresh from 'utils/generateTokenNeedRefresh';

export function* loginUser(action) {
  // const requestURL = `${API_ROOT}/register/`;
  const requestURL = `${API_ROOT}/login/`;
  const { history, data } = action;

  try {
    const res = yield call(axiosRequest, {
      url: requestURL,
      method: 'post',
      data,
    });

    yield put(loginSuccess({ res }));
    //Save Token
    localStorage.setItem('token', res.token);
    generateTokenNeedRefresh();
    //go to dashboard
    history.push('/dashboard');
  } catch (err) {
    yield put(error(err.response.data));
  }
}

export default function* listen() {
  yield takeLatest(START_LOGIN, loginUser);
}
