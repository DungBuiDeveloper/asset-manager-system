/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import { Redirect } from 'react-router-dom';
export function HomePage({}) {
  return <Redirect to={{ pathname: '/dashboard' }} />;
}

export default HomePage;
