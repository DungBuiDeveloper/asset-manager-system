import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import axios from 'axios';
const PrivateRoute = ({
  component: Component,
  authenticated,
  layout: Layout,
  ...rest
}) => {
  return (
    <div>
      <Route
        {...rest}
        render={props =>
          localStorage.getItem('token') ? (
            <Layout component={Component} />
          ) : (
            <Redirect
              to={{ pathname: '/login', state: { from: props.location } }}
            />
          )
        }
      />
    </div>
  );
};

export default PrivateRoute;
