import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core';
import NavBar from './NavBar';
import TopBar from './TopBar';

import { connect } from 'react-redux';

import { withRouter } from 'react-router-dom';
import { compose } from 'redux';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';

import reducer from '../reducer';

import saga from '../saga';
import { startGet } from '../actions';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    display: 'flex',
    height: '100%',
    overflow: 'hidden',
    width: '100%',
  },
  wrapper: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
    paddingTop: 64,
    paddingBottom: 20,
    [theme.breakpoints.up('lg')]: {
      paddingLeft: 256,
    },
  },
  contentContainer: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
  },
  content: {
    width: '100%',
    height: '100%',
  },
}));
const key = 'global';
const DashboardLayout = ({
  currentUser,
  startGet,
  component: Component,
  ...rest
}) => {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  const classes = useStyles();
  let [isMobileNavOpen, setIsMobileNavOpen] = useState(false);

  useEffect(() => {
    if (currentUser == null) {
      startGet();
    }
  }, []);

  return (
    <div className={classes.root}>
      <TopBar onMobileNavOpen={setIsMobileNavOpen} />
      {currentUser ? (
        <NavBar
          currentUser={currentUser}
          onMobileNavOpen={setIsMobileNavOpen}
          openMobile={isMobileNavOpen}
        />
      ) : null}

      <div className={classes.wrapper}>
        <div className={classes.contentContainer}>
          <div className={classes.content}>
            <Component {...rest} />
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    currentUser: state.global.currentUser,
  };
};

export function mapDispatchToProps(dispatch) {
  return {
    startGet: () => dispatch(startGet()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default withRouter(compose(withConnect)(DashboardLayout));
