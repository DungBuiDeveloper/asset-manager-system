import React from 'react';

import { Link } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { ListItem, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  item: {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0,
  },
  button: {
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightMedium,
    justifyContent: 'flex-start',
    letterSpacing: 0,
    padding: '10px 8px',
    textTransform: 'none',
    width: '100%',
  },
  icon: {
    marginRight: theme.spacing(1),
    color:'#000'
  },
  title: {
    marginRight: 'auto',
    color:'#000'
  },
  titleActive:{
    color:'#0062cc'
  },
  iconActive:{
    color:'#0062cc',
    marginRight: theme.spacing(1),
  },
  active: {
    color: theme.palette.primary.main,
    '& $title': {
      fontWeight: theme.typography.fontWeightMedium,
    },
    '& $icon': {
      color: theme.palette.primary.main,
    },
  },
}));

const NavItem = ({ className, href, icon: Icon, title, ...rest }) => {
  const classes = useStyles();
  let currentUrl = window.location.href;
  var checkUrl = currentUrl.search(href);
  return (
    <ListItem
      className={clsx(classes.item, className)}
      disableGutters
      {...rest}
    >
      <Link className={classes.button} to={href}>
        {Icon && <Icon className={checkUrl > 0 ? (classes.iconActive) : classes.icon} size="20" />}
        <span className={checkUrl > 0 ? (classes.title , classes.titleActive) : classes.title}  >{title}</span>
      </Link>
    </ListItem>
  );
};

NavItem.propTypes = {
  className: PropTypes.string,
  href: PropTypes.string,
  icon: PropTypes.elementType,
  title: PropTypes.string,
};

export default NavItem;
