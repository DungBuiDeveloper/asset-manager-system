/**
 * Gets the repositories of the user from Github
 */

import { call, put, takeLatest } from 'redux-saga/effects';

import { START_GET } from './constants';
import { axiosRequest } from '../../utils/request';
import HandleError from '../../utils/handleError';
import { get, error } from './actions';

/**
 * Github repos request/response handler
 */
export function* gets() {
  const requestURL = `/me`;
  try {
    // Call our request helper (see 'utils/request')
    const data = yield call(axiosRequest, {
      url: requestURL,
      method: 'get',
      auth: true,
    });

    yield put(get(data));
  } catch (err) {
    HandleError(err);
    yield put(error(err.message));
  }
}

export default function* listen() {
  yield takeLatest(START_GET, gets);
}
