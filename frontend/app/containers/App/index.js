/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';

import { Route } from 'react-router-dom';
import Switch from 'react-router-transition-switch';
import Fader from 'react-fader';
import HomePage from 'containers/HomePage/Loadable';

import FeaturePage from 'containers/FeaturePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

import './styles';

import PrivateRoute from './PrivateRoute';
import ScrollToTop from './ScrollToTop';

// Dashboard
import DashboardPage from 'containers/DashboardPage/Loadable';

//ManufacturersPage
import ManufacturersPage from 'containers/ManufacturersPage/Loadable';
import ManufacturersAdd from 'containers/ManufacturersPage/add/Loadable';
import ManufacturersEdit from 'containers/ManufacturersPage/edit/Loadable';

//Auth
import LoginPage from 'containers/LoginPage/Loadable';
import RegisterPage from 'containers/RegisterPage/Loadable';
import Layout from './Layout/';

export default function App() {
  return (
    <div>
      <ScrollToTop />

      <Switch component={Fader}>
        {/* =======================  Auth Route  ======================== */}

        <Route exact path="/login" component={LoginPage} />
        <Route exact path="/register" component={RegisterPage} />

        {/* =======================  Auth Route  ======================== */}

        {/* =======================  Public Route  ======================== */}

        <Route exact path="/features" component={FeaturePage} />
        <Route exact path="/terms-and-conditions" component={FeaturePage} />

        {/* =======================  Public Route  ======================== */}

        {/**
         * =======================  Private Route  ========================
         * [Private Route Access only Admin or persimmon allowed]
         */}

        <PrivateRoute exact path="/" layout={Layout} component={HomePage} />
        <PrivateRoute exact path="/manufacturers" layout={Layout} component={ManufacturersPage} />
        <PrivateRoute exact path="/manufacturers/add" layout={Layout} component={ManufacturersAdd} />
        <PrivateRoute exact path="/manufacturers/edit/:id" layout={Layout} component={ManufacturersEdit} />
        <PrivateRoute exact path="/dashboard" layout={Layout} component={DashboardPage} />

        {/* * =======================  Private Route  ======================== * */}


        {/* =========  404 Route ========= */}
        <Route path="" component={NotFoundPage} />
        {/* =========  404 Route ========= */}
      </Switch>
    </div>
  );
}
