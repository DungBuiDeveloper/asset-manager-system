import { START_GET, GET_CURENT_USER, ERROR } from './constants';
export function startGet() {
  return {
    type: START_GET,
  };
}

export function get(data) {
  return {
    type: GET_CURENT_USER,
    payload: data,
  };
}

export function error(error) {
  return {
    type: ERROR,
    error,
  };
}
