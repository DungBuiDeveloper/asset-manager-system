import React, { useState, useRef } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles,
  TableSortLabel,
  TableContainer,
} from '@material-ui/core';

import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Pagination from '@material-ui/lab/Pagination';
import Dialog from 'components/Dialog';
const useStyles = makeStyles(theme => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2),
  },
}));

const Results = ({
  className,
  data,
  PageChange,
  deleteRow,
  LimitChange,
  goEdit,
  ...rest
}) => {
  const deleteButtonRef = useRef();
  const classes = useStyles();
  const [selectedDocs, setSelectedDocs] = useState([]);

  const handleSelectAll = event => {
    let ids;

    if (event.target.checked) {
      ids = data.docs.map(doc => doc._id);
    } else {
      ids = [];
    }

    setSelectedDocs(ids);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedDocs.indexOf(id);
    let ids = [];

    if (selectedIndex === -1) {
      ids = ids.concat(selectedDocs, id);
    } else if (selectedIndex === 0) {
      ids = ids.concat(selectedDocs.slice(1));
    } else if (selectedIndex === selectedDocs.length - 1) {
      ids = ids.concat(selectedDocs.slice(0, -1));
    } else if (selectedIndex > 0) {
      ids = ids.concat(
        selectedDocs.slice(0, selectedIndex),
        selectedDocs.slice(selectedIndex + 1),
      );
    }

    setSelectedDocs(ids);
  };

  const handleLimitChange = event => {
    LimitChange({ page: data.page, limit: event.target.value });
  };

  const handlePageChange = (event, newPage) => {
    if (newPage > 0) {
      PageChange(newPage + 1);
    }
  };

  const handlePageChangeNumber = (event, newPage) => {
    if (newPage > 0) {
      PageChange(newPage);
    }
  };

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Dialog
        onClickOk={deleteRow}
        title={'DELETE'}
        body={'Are you sure you want to DELETE this row?'}
        ref={deleteButtonRef}
      />

      <PerfectScrollbar>
        <Box>
          <TablePagination
            component="div"
            onChangePage={handlePageChange}
            onChangeRowsPerPage={handleLimitChange}
            rowsPerPage={data.limit}
            page={data.page - 1} // fixed warning range out when data smaller data.limit pagination will start from 0
            count={data.totalDocs}
            rowsPerPageOptions={[20, 50, 100]}
          />
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell padding="checkbox">
                    <Checkbox
                      checked={selectedDocs.length === data.docs.length}
                      color="primary"
                      indeterminate={
                        selectedDocs.length > 0 &&
                        selectedDocs.length < data.docs.length
                      }
                      onChange={handleSelectAll}
                    />
                  </TableCell>
                  <TableCell>
                    <TableSortLabel>Name</TableSortLabel>
                  </TableCell>

                  <TableCell>url</TableCell>
                  <TableCell>suportEmail</TableCell>
                  <TableCell>suportPhone</TableCell>
                  <TableCell>suportUrl</TableCell>
                  <TableCell>Updated</TableCell>
                  <TableCell>Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data
                  ? data.docs.map(doc => (
                      <TableRow
                        hover
                        key={doc._id}
                        selected={selectedDocs.indexOf(doc._id) !== -1}
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={selectedDocs.indexOf(doc._id) !== -1}
                            onChange={event => handleSelectOne(event, doc._id)}
                            value="true"
                          />
                        </TableCell>

                        <TableCell>
                          <Box alignItems="center" display="flex">
                            <Avatar className={classes.avatar} src={doc.thumb}>
                              {doc.name}
                            </Avatar>
                            <Typography color="textPrimary" variant="body1">
                              {doc.name}
                            </Typography>
                          </Box>
                        </TableCell>

                        <TableCell>
                          <a target="_blank" href={doc.url}>
                            {doc.url}
                          </a>
                        </TableCell>

                        <TableCell>
                          <a target="_blank" href={`mailto:${doc.suportEmail}`}>
                            {doc.suportEmail}
                          </a>
                        </TableCell>

                        <TableCell>
                          <a target="_blank" href={`tel:${doc.suportPhone}`}>
                            {doc.suportPhone}
                          </a>
                        </TableCell>

                        <TableCell>
                          <a target="_blank" href={doc.suportUrl}>
                            {doc.suportUrl}
                          </a>
                        </TableCell>

                        <TableCell>
                          {moment(doc.updatedAt).format('DD/MM/YYYY')}
                        </TableCell>

                        <TableCell>
                          <div
                            className="btn-group"
                            role="group"
                            aria-label="Basic example"
                          >
                            <button
                              onClick={() => {
                                deleteButtonRef.current.handleClickOpen(
                                  doc._id,
                                );
                              }}
                              className="btn btn-danger btn-sm"
                            >
                              <DeleteIcon size={'small'} />
                            </button>

                            <button 
                              className="btn btn-primary btn-sm"
                              onClick={ ()=> goEdit(doc._id) }
                            >
                              <EditIcon size={'small'} />
                            </button>
                          </div>
                        </TableCell>
                      </TableRow>
                    ))
                  : null}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </PerfectScrollbar>

      <Box p={3}>
        <Pagination
          hidePrevButton
          hideNextButton
          page={data.page}
          onChange={handlePageChangeNumber}
          component="div"
          count={data.totalDocs}
          variant="outlined"
          shape="rounded"
          color="secondary"
        />
      </Box>
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  data: PropTypes.object.isRequired,
  PageChange: PropTypes.func,
  LimitChange: PropTypes.func,
  deleteRow: PropTypes.func,
};

export default Results;
