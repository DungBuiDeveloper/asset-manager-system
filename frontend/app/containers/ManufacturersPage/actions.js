import {
  GET,
  START_GET,
  DELETE,
  START_DELETE,
  POST,
  START_POST,
  GET_ID,
  START_GET_ID,
  PATCH,
  START_PATCH,
  ERROR,
} from './constants';

export function startGet(option) {
  return {
    type: START_GET,
    option,
  };
}

export function get(data) {
  return {
    type: GET,
    data,
  };
}

export function startGetId(data) {
  return {
    type: START_GET_ID,
    data,
  };
}

export function getId(data) {
  return {
    type: GET_ID,
    data,
  };
}

export function error(err) {
  return {
    type: ERROR,
    error: err,
  };
}

export function startDelete(op) {
  return {
    type: START_DELETE,
    id: op.id,
    url: op.url,
    close: op.close,
  };
}

export function deleteId(data) {
  return {
    type: DELETE,
    id: data.id,
  };
}

export function post(data) {
  const history = history;
  return {
    type: POST,
    data,
    history,
  };
}

export function startPost(data) {
  return {
    type: START_POST,
    data,
  };
}

export function startPatch(data, id) {
  const history = history;
  return {
    type: START_PATCH,
    data,
    history,
    id,
  };
}

export function patch(data) {
  return {
    type: PATCH,
    data: data.res,
  };
}
