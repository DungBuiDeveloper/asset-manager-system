import { createSelector } from 'reselect';
import { initialState } from './reducer';

const select = state => state.manufacturer || initialState;

const makeSelect = () =>
  createSelector(
    select,
    state => state.data,
  );
const makeSelectDetail = () =>
  createSelector(
    select,
    state => state.detail,
  );
const makeSelectLoading = () =>
  createSelector(
    select,
    state => state.loading,
  );
const makeSelectError = () =>
  createSelector(
    select,
    state => state.error,
  );
const makeSelectDataId = () =>
  createSelector(
    select,
    state => state.detail,
  );
export {
  select,
  makeSelectDetail,
  makeSelectLoading,
  makeSelectError,
  makeSelect,
  makeSelectDataId
};
