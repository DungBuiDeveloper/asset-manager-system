/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'RegisterPage';

export default defineMessages({
  NOT_A_VALID_URL: {
    id: `${scope}.NOT_A_VALID_URL`,
    defaultMessage: 'Email already exists',
  },
});
