export const GET = 'manufacturers/get';
export const START_GET = 'manufacturers/START_GET';

export const DELETE = 'boilerplate/manufacturers/DELETE';
export const START_DELETE = 'manufacturers/START_DELETE';

export const POST = 'boilerplate/manufacturers/POST';
export const START_POST = 'boilerplate/manufacturers/START_POST';

export const GET_ID = 'boilerplate/manufacturers/GET_ID';
export const START_GET_ID = 'boilerplate/manufacturers/START_GET_ID';

export const PATCH = 'boilerplate/manufacturers/PATCH';
export const START_PATCH = 'boilerplate/manufacturers/LOADING_EDIT_BLOG';

export const CHANGE_IMAGE = 'boilerplate/manufacturers/CHANGE_IMAGE';

export const ERROR = 'boilerplate/manufacturers/ERROR';
