/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import {
  GET,
  START_GET,
  DELETE,
  START_DELETE,
  POST,
  START_POST,
  GET_ID,
  START_GET_ID,
  PATCH,
  START_PATCH,
  ERROR,
  CHANGE_IMAGE
} from './constants';

// The initial state of the App
export const initialState = {
  data: null,
  detail: null,
  loading: false,
  error: null,
};

function removeItemAll(arr, value) {
  var i = 0;

  while (i < arr.docs.length) {
    if (arr.docs[i]._id === value) {
      arr.docs.splice(i, 1);
    } else {
      ++i;
    }
  }

  return arr;
}

/* eslint-disable default-case, no-param-reassign */
const Reducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET:
        draft.loading = false;
        draft.data = action.data;
        break;
      case START_DELETE:
        draft.loading = true;
        break;
      case DELETE:
        draft.data = removeItemAll(state.data, action.id);
        draft.loading = false;
        break;

      case ERROR:
        draft.loading = false;
        draft.error = action.error.msg;
        break;

      case START_POST:
        draft.loading = true;
        break;
      case POST:
        draft.loading = false;
        break;

      case START_GET_ID:
        draft.loading = true;
        break;
      case GET_ID:
        draft.detail = action.data;
        draft.loading = false;
        break;
      case START_PATCH:
        draft.loading = true;
        break;
      case PATCH:
        draft.loading = false;
        break;
      case CHANGE_IMAGE:
        let detail = state.detail;
        detail.thumb = action.url;
        draft.detail = detail;
        break;
    }
  });
export default Reducer;
