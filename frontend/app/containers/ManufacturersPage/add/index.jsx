import React, { memo } from 'react';
import { Box, Container } from '@material-ui/core';

import PropTypes from 'prop-types';

import { compose } from 'redux';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import {  makeSelectError, makeSelectLoading } from '../selectors';
import reducer from '../reducer';
import { connect } from 'react-redux';
import saga from '../saga';
import { startPost } from '../actions';

import { withRouter } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';

import Alert from 'components/Alert';
import LoadingIndicator from 'components/LoadingIndicator';
import { FormattedMessage } from 'react-intl';
import messages from '../messages';
import { CardContent, Typography, Card } from '@material-ui/core';
import Form from './Form';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  coverBox: {
    position: 'relative',
    marginBottom: '65px',
    paddingTop: '95px',
  },
  boderCover: {
    borderBottom: '1px solid #ddd',
    position: 'absolute',
    width: '100%',
    left: 0,
    top: 0,
    padding: '15px',
  },
});
const key = 'manufacturer';

function Manufacturers({ loading, error, startPost , history }) {
  const classes = useStyles();

  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  const submitForm = function(data){
   
    startPost({
      data,
      url:'/manufacturers',
      history
    });
  }

  const renderError = (error) => {
    
    if(error && typeof error == 'object'){
      error.map((item,i)=>{
        console.log(item.msg);
        return (
           <Alert
            error={true}
            message={<FormattedMessage {...messages[item.msg]} />}
          />
        )
      })
    }
  }
  

  
 
  return (
    <div className="root-module">
      <Container maxWidth={false}>

        {loading ? <LoadingIndicator /> : null}

        <Box mt={3}>
          <Card>
            <CardContent className={classes.coverBox}>
              <div className={classes.boderCover}>
                <Typography color="textPrimary" variant="h2">
                  <span>
                    Manufacturers{' '}
                    <Typography
                      color="textSecondary"
                      gutterBottom
                      variant="body2"
                    >
                      Add
                    </Typography>
                  </span>
                </Typography>
              </div>

              <Form submitForm={submitForm}/>
              {/* Form */}

            </CardContent>
          </Card>
        </Box>
      </Container>
    </div>
  );
}

Manufacturers.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
  startPost: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    startPost: data => dispatch(startPost(data))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default withRouter(
  compose(
    withConnect,
    memo,
  )(Manufacturers),
);
