
import { url } from 'utils/validate';

const validate = values => {



  const errors = {};

  
  if (!values.name) {
    errors.name = 'Required';
  }

  if (!values.url) {
    errors.url = 'Required';
  }else {
    if(url(values.url) !== true){
      errors.url = 'url valid';
    }
  }

  if (!values.suportUrl) {
    errors.suportUrl = 'Required';
  }else {
    if(url(values.suportUrl) !== true){
      errors.suportUrl = 'url valid';
    }
  }

  if (!values.suportPhone) {
    errors.suportPhone = 'Required';
  }

  if (!values.suportEmail) {
    errors.suportEmail = 'Required';
  }


  return errors;
};

export default validate;
