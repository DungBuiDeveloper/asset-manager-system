import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  makeStyles,
} from '@material-ui/core';

import AddIcon from '@material-ui/icons/Add';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import Grid from '@material-ui/core/Grid';
import _ from 'lodash';
import { Link as RouterLink } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {},
  importButton: {
    marginRight: theme.spacing(1),
  },
  exportButton: {
    marginRight: theme.spacing(1),
  },
}));

const Toolbar = ({ search, history, className, ...rest }) => {
  const classes = useStyles();
  const [searchTerm, setSearchTerm] = useState('');
  const debounceSearch = useRef(
    _.debounce(searchTerm => {
      search(searchTerm);
    }, 1000),
  );

  useEffect(
    () => {
      if (searchTerm) {
        debounceSearch.current(searchTerm);
      }
    },
    [searchTerm], // Only call effect if debounced search term changes
  );

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Box mt={3}>
        <Card>
          <CardContent>
            <Grid container spacing={3}>
              <Grid item xs={3}>
                <Box>
                  <TextField
                    onChange={e => setSearchTerm(e.target.value)}
                    fullWidth
                    placeholder="Search"
                  />
                </Box>
              </Grid>

              <Grid item xs={9}>
                <Box display="flex" justifyContent="flex-end">
                  <Button variant="contained" className={classes.importButton}>
                    <ImportExportIcon />
                  </Button>
                  <Button variant="contained" className={classes.exportButton}>
                    <CloudDownloadIcon />
                  </Button>

                  <RouterLink to="/manufacturers/add">
                    <Button color="primary" variant="contained">
                      <AddIcon />
                    </Button>
                  </RouterLink>
                </Box>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Box>
    </div>
  );
};

Toolbar.propTypes = {
  className: PropTypes.string,
  search: PropTypes.func,
};

export default Toolbar;
