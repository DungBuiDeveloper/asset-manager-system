import React, { memo, useEffect } from 'react';
import { Box, Container } from '@material-ui/core';

import PropTypes from 'prop-types';

import { compose } from 'redux';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import { connect } from 'react-redux';

import { withRouter } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import LoadingIndicator from 'components/LoadingIndicator';

import Alert from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import { startGet, startDelete } from './actions';
import saga from './saga';
import reducer from './reducer';
import Toolbar from './Toolbar';
import { makeSelect, makeSelectError, makeSelectLoading } from './selectors';
import TableData from './TableData';
import messages from './messages';

const key = 'manufacturer';

function Manufacturers({ loading, error, data, getData, deleteData, history }) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    getData({ url: '/manufacturers' });
  }, []);

  if (data == null) {
    return <LoadingIndicator />;
  }

  function PageChange(newPage) {
    getData({ url: `/manufacturers?page=${newPage}` });
  }

  function LimitChange(params) {
    getData({
      url: `/manufacturers?page=${params.page}&&limit=${params.limit}`,
    });
  }

  function search(searchTerm) {
    getData({
      url: `/manufacturers?filter=${searchTerm}&&fields=name,url,suportUrl,suportPhone,suportEmail`,
    });
  }

  function goEdit(id) {
    history.push(`/manufacturers/edit/${id}`);
  }

  return (
    <div className="root-module">
      <Container maxWidth={false}>
        {error != null ? (
          <Alert error message={<FormattedMessage {...messages[error]} />} />
        ) : null}

        {loading ? <LoadingIndicator /> : null}
        <Toolbar search={search} />
        <Box mt={3}>
          <TableData
            PageChange={PageChange}
            goEdit={goEdit}
            deleteRow={deleteData}
            LimitChange={LimitChange}
            data={data}
          />
        </Box>
      </Container>
    </div>
  );
}

Manufacturers.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
  data: PropTypes.object,
  getData: PropTypes.func,
  deleteData: PropTypes.func,
  history: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  data: makeSelect(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    getData: option => dispatch(startGet(option)),
    deleteData: option => dispatch(startDelete(option)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default withRouter(
  compose(
    withConnect,
    memo,
  )(Manufacturers),
);
