/**
 * Gets the repositories of the user from Github
 */

import { call, put, takeLatest } from 'redux-saga/effects';

import {
  START_GET,
  START_DELETE,
  START_POST,
  START_GET_ID,
  START_PATCH,
} from './constants';
import { axiosRequest } from 'utils/request';
import { get, deleteId, error , post , getId, patch } from './actions';
import HandleError from '../../utils/handleError';

export function* gets(action) {
  try {
    // Call our request helper (see 'utils/request')
    const data = yield call(axiosRequest, {
      url: action.option.url,
      method: 'get',
      auth: true,
    });

    yield put(get(data));
  } catch (err) {
    HandleError(err);
    yield put(error(err.message));
  }
}

export function* deleteRow(action) {
  try {
    const data = yield call(axiosRequest, {
      url: action.url,
      method: 'delete',
      auth: true,
    });
    action.close();
    data.id = action.id;
    yield put(deleteId(data));
  } catch (err) {
    HandleError(err);

    action.close();
    yield put(error(err.message));
  }
}


export function* postSaga(action) {
  try {
    const {history} = action.data;
    const data = yield call(axiosRequest, {
      url: action.data.url,
      method: 'post',
      auth: true,
      data:action.data.data
    });
    
    yield put(post(data));
    history.push('/manufacturers');
  } catch (err) {
    yield put(error(err.response.data.errors));
    HandleError(err);
    
  }
}

export function* getDataEdit(action) {
  try {
    const data = yield call(axiosRequest, {
      url: action.data.url,
      method: 'get',
      auth: true,
    });
    yield put(getId(data));
 
  } catch (err) {
    yield put(error(err.response.data.errors));
    HandleError(err);
  }
}
export function* patchSaga(action) {
  console.log('saga');
  try {
    const {history} = action.data;
    const data = yield call(axiosRequest, {
      url: action.data.url,
      method: 'patch',
      auth: true,
      data:action.data.data
    });
    
    yield put(patch(data));
    history.push('/manufacturers');
  } catch (err) {
    yield put(error(err.response.data.errors));
    HandleError(err);
    
  }
}
export default function* listen() {
  yield takeLatest(START_GET, gets);
  yield takeLatest(START_POST, postSaga);
  yield takeLatest(START_DELETE, deleteRow);
  yield takeLatest(START_GET_ID, getDataEdit);
  yield takeLatest(START_PATCH, patchSaga);
}
