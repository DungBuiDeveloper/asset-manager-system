import React, { useEffect, useState } from 'react';

import { Field, reduxForm  } from 'redux-form';
import renderFieldInput from 'components/reduxForm/input';
import RenderDropzone from 'components/reduxForm/DropZone';
import { scrollToFirstError } from 'components/scrollToFirstError';

import validate from './validate';
import { change } from 'redux-form';
import {CHANGE_IMAGE} from 'containers/ManufacturersPage/constants.js';

const FormEdit = ({
  handleSubmit,
  pristine,
  submitting,
  submitForm,
  dispatch,
  dataEdit,
  initialize
}) => {
  const setUrlImage = function(url) {
    dispatch(change('manufacturersFormEdit', 'thumb', url));
    dispatch({
      type: CHANGE_IMAGE,
      url
    });

  };
  useEffect(() => {
    console.log(dataEdit);
    initialize(dataEdit);
  },[]);
  


  return (
    <div id="login">
      <form onSubmit={handleSubmit(data => submitForm(data))}>
        <Field
          label={'Name'}
          name="name"
          component={renderFieldInput}
          type="text"
          placeholder="Name"
          nameError="name"
        />
        {/* Name */}

        <Field
          label={'Url'}
          name="url"
          component={renderFieldInput}
          type="text"
          placeholder="Url"
          nameError="url"
        />
        {/* Url */}

        <Field
          label={'Suport Url'}
          name="suportUrl"
          component={renderFieldInput}
          type="text"
          placeholder="Suport Url"
          nameError="suportUrl"
        />
        {/* suportUrl */}

        <Field
          label={'Suport Phone'}
          name="suportPhone"
          component={renderFieldInput}
          type="text"
          placeholder="Suport Phone"
          nameError="suportPhone"
        />
        {/* suportUrl */}

        <Field
          label={'Suport Email'}
          name="suportEmail"
          component={renderFieldInput}
          type="text"
          placeholder="Suport Email"
          nameError="suportEmail"
        />
        {/* suportUrl */}

        <div className="dropzone">
          {/*DropZone*/}
          <RenderDropzone
            thumbDefault={[dataEdit.thumb]}
            changeUrl={setUrlImage} 
          />

          {/*DropZone*/}
          <Field
            style={{ width: '100%' }}
            name="thumb"
            component="input"
            type="hidden"
          />
        </div>
        {/* Thumb */}

        <div className={'footerCard'}>
          <button type="button" className="btn btn-sedary btn-sm">
            CANCEL
          </button>
          <button
            className="btn btn-primary btn-sm"
            disabled={pristine || submitting}
            size="large"
            type="submit"
            variant="contained"
          >
            SUBMIT
          </button>
        </div>
      </form>
    </div>
  );
};

export default reduxForm({
  form: 'manufacturersFormEdit',
  validate,
  onSubmitFail: errors => scrollToFirstError(errors),
})(FormEdit);
