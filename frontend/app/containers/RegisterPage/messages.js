/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'RegisterPage';

export default defineMessages({
  EMAIL_ALREADY_EXISTS: {
    id: `${scope}.EMAIL_ALREADY_EXISTS`,
    defaultMessage: 'Email already exists',
  },
});
