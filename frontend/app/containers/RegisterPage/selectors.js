/**
 * Blogpage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectReg = state => state.register || initialState;

const makeSelectLoading = () =>
  createSelector(
    selectReg,
    state => state.loading,
  );
const makeSelectError = () =>
  createSelector(
    selectReg,
    state => state.error,
  );

export { selectReg, makeSelectLoading, makeSelectError };
