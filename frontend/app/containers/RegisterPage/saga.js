import { call, put, takeLatest } from 'redux-saga/effects';

import { START_REG } from './constants';
import { axiosRequest } from 'utils/request';

import { error, regSuccess } from './actions';

import generateTokenNeedRefresh from 'utils/generateTokenNeedRefresh';

export function* regUser(action) {
  const requestURL = `register/`;
  const { history, data } = action;

  let formatData = data;
  formatData.name = `${data.lastName} ${data.firstName}`;

  try {
    const res = yield call(axiosRequest, {
      url: requestURL,
      method: 'post',
      data: formatData,
    });

    yield put(regSuccess({ res }));
    //Save Token
    localStorage.setItem('token', res.token);
    generateTokenNeedRefresh();
    // go to dashboard
    history.push('/dashboard');
  } catch (err) {
    yield put(error(err.response.data));
  }
}

export default function* listen() {
  yield takeLatest(START_REG, regUser);
}
