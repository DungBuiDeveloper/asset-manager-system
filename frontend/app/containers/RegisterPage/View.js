import React, { useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';

import { Box, Button, Container, Link, Typography } from '@material-ui/core';
import { Field, reduxForm } from 'redux-form';
import renderFieldInput from 'components/reduxForm/input';
import renderFieldCheckBox from 'components/reduxForm/checkbox';
import validate from './validate';
import { change } from 'redux-form';

const RegisterView = ({
  handleSubmit,
  pristine,
  submitting,
  submitForm,
  dispatch,
}) => {
  useEffect(() => {
    dispatch(change('registerForm', 'policy', false));
  }, []);

  const setPolicy = function(status) {
    setTimeout(function() {
      dispatch(change('registerForm', 'policy', !status));
    }, 100);
  };

  return (
    <div className="center_auth" id="Register">
      <Box
        display="flex"
        flexDirection="column"
        height="100%"
        justifyContent="center"
      >
        <Container maxWidth="sm">
          <form onSubmit={handleSubmit(data => submitForm(data))}>
            <Box mb={3}>
              <Typography color="textPrimary" variant="h2">
                Create new account
              </Typography>
              <Typography color="textSecondary" gutterBottom variant="body2">
                Use your email to create new account
              </Typography>
            </Box>

            <Field
              label={'First name'}
              name="firstName"
              component={renderFieldInput}
              type="text"
              placeholder="First name"
              nameError="first_name"
            />

            {/* First name */}

            <Field
              label={'Last name'}
              name="lastName"
              component={renderFieldInput}
              type="text"
              placeholder="Last name"
              nameError="last_name"
            />

            {/* Last name */}

            <Field
              label={'Email Address'}
              name="email"
              component={renderFieldInput}
              type="email"
              placeholder="Email Address"
              nameError="last_name"
            />

            {/* Email */}

            <Field
              label={'Password'}
              name="password"
              component={renderFieldInput}
              type="password"
              placeholder="Password"
              nameError="password"
            />

            {/* Password */}

            <Field
              label={'Re-Password'}
              name="re_password"
              component={renderFieldInput}
              type="password"
              placeholder="Re-Password"
              nameError="re_password"
            />

            {/* Password */}
            <Box alignItems="center" display="flex" ml={-1}>
              <Field
                name="policy"
                onChange={setPolicy}
                component={renderFieldCheckBox}
                label={'I have read the'}
                nameError="policy"
              />
              <Typography color="textSecondary" variant="body1">
                {' '}
                <Link
                  color="primary"
                  component={RouterLink}
                  to="/terms-and-conditions"
                  underline="always"
                  variant="h6"
                >
                  Terms and Conditions
                </Link>
              </Typography>
            </Box>

            <Box my={2}>
              <Button
                disabled={pristine || submitting}
                color="primary"
                fullWidth
                size="large"
                type="submit"
                variant="contained"
              >
                Sign up now
              </Button>
            </Box>
            <Typography color="textSecondary" variant="body1">
              Have an account?{' '}
              <Link component={RouterLink} to="/login" variant="h6">
                Sign in
              </Link>
            </Typography>
          </form>
        </Container>
      </Box>
    </div>
  );
};

export default reduxForm({
  form: 'registerForm',
  validate,
})(RegisterView);
