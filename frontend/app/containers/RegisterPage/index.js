import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import View from './View';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';

import { regStart } from './actions';
import saga from './saga';
import reducer from './reducer';

import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { makeSelectError, makeSelectLoading } from './selectors';
import { createStructuredSelector } from 'reselect';
import LoadingIndicator from 'components/LoadingIndicator';
import Alert from 'components/Alert';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
const key = 'register';

export function RegisterPage({ loading, error, history, regStart }) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  function submitForm(data) {
    regStart(data, history);
  }

  return (
    <div>
      <Helmet>
        <title>Create new account</title>
        <meta name="description" content="Create new account Asset Manager" />
      </Helmet>
      {error != null ? (
        <Alert
          error={true}
          message={<FormattedMessage {...messages[error]} />}
        />
      ) : null}

      {loading ? <LoadingIndicator /> : null}
      <View submitForm={submitForm} />
    </div>
  );
}

RegisterPage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  regStart: PropTypes.func,
  history: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    regStart: (data, history) => dispatch(regStart(data, history)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default withRouter(
  compose(
    withConnect,
    memo,
  )(RegisterPage),
);
