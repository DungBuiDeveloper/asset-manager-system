const validate = values => {
  const errors = {};
  if (!values.firstName) {
    errors.firstName = 'Required';
  }
  if (!values.lastName) {
    errors.lastName = 'Required';
  }
  if (!values.password) {
    errors.password = 'Required';
  }
  if (!values.re_password) {
    errors.re_password = 'Required';
  }
  if (values.re_password != values.password) {
    errors.re_password = 'not matched';
  }
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (values.policy != true) {
    errors.policy = 'you are not confirm';
  }

  return errors;
};

export default validate;
