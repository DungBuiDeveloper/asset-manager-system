/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import { LOADING, ERROR, START_REG, SUCCESS_REG } from './constants';

export function loading() {
  return {
    type: LOADING,
  };
}

export function error(error) {
  let eMessage = error.errors.msg;
  return {
    type: ERROR,
    error: eMessage,
  };
}

export function regStart(data, history) {
  return {
    type: START_REG,
    data,
    history,
  };
}

export function regSuccess(data) {
  return {
    type: SUCCESS_REG,
    data: data.res,
  };
}
