/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import { LOADING, ERROR, START_REG, SUCCESS_REG } from './constants';

// The initial state of the App
export const initialState = {
  loading: false,
  error: null,
};

/* eslint-disable default-case, no-param-reassign */
const Reducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case START_REG:
        draft.loading = true;
        break;
      case ERROR:
        draft.loading = false;
        draft.error = action.error;
        break;
      case SUCCESS_REG:
        draft.loading = false;
        break;
      case LOADING:
        draft.loading = true;
        draft.error = null;
        break;
    }
  });
export default Reducer;
