import React from 'react';

import FormHelperText from '@material-ui/core/FormHelperText'
const errorInputMessage = ({message}) => (

    <FormHelperText>
        <span className="text-danger">{message}</span>
    </FormHelperText> 
);

export default errorInputMessage;
