import React, { Component } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import ErrorInput from './errorInput';


// useEffect(() => {
//   dispatch(change('manufacturersForm', 'sex', 1));
// }, []);

// const radioData = [
//   {
//     label:'name',
//     value:1,
//   },
//   {
//     label:'name2',
//     value:2,
//   }
// ]

const radioButton = ({ input , data, meta: { touched, error } , ...rest }) => (
    <FormControl component="fieldset">
      <RadioGroup   {...input} {...rest}>
        {
          data.map((item,i)=>{
            return (
              
                <FormControlLabel checked={item.value == input.value } key={i} value={item.value} control={<Radio />} label={item.label} />
              
            )
          })
        }
        
      
      </RadioGroup>
      
      { touched && error && <ErrorInput message={error} /> } 
       
        
      
    </FormControl>
)
  export default radioButton;