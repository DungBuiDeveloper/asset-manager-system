import React, { Component } from 'react';
import Select from 'react-select';

export default class SelectCustom extends Component {
  onChange(event) {
    if (this.props.input.onChange && event != null) {
      this.props.input.onChange(event);
    } else {
      this.props.input.onChange(null);
    }
  }

  render() {
    const { input, options, name, id, ...custom } = this.props;
    console.log(this.props.input.value);
    return (
      <div style={{ position: 'relative', zIndex: 99999 }}>
        <Select
          {...input}
          {...custom}
          id={id}
          name={name}
          options={options}
          value={this.props.input.value || ''}
          onBlur={() => this.props.input.onBlur(input.value)}
          onChange={this.onChange.bind(this)}
        />
      </div>
    );
  }
}
