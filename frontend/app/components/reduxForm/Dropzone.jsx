import React, { useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import { axiosRequest } from 'utils/request';
import { API_ROOT } from 'utils/axios/constants';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
function generateImageName(orginalName) {
  var result, i, j;
  result = '';
  for (j = 0; j < 32; j++) {
    if (j == 8 || j == 12 || j == 16 || j == 20) result = result + '-';
    i = Math.floor(Math.random() * 16)
      .toString(16)
      .toUpperCase();
    result = result + i;
  }
  return result + orginalName;
}

export default function Previews(props) {
  const [files, setFiles] = useState([]);
  const [dragEnter, setDragEnter] = useState(false);
  const { getRootProps, getInputProps, fileRejections } = useDropzone({
    accept: 'image/png,image/jpg,image/jpeg',
    maxFiles: 1,
    maxSize: 1024 * 1024 * 2,
    onDragEnter: e => {
      setDragEnter(true);
    },
    onDragLeave: e => {
      setDragEnter(false);
    },

    onDrop: (acceptedFiles, fileRejections) => {
      if (fileRejections.length != 0) {
        alert(fileRejections[0].errors[0].message);
        return false;
      }
      setFiles(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      );

      var form = new FormData();

      form.append(
        'file',
        acceptedFiles[0],
        generateImageName(acceptedFiles[0].name),
      );
      var option = {
        url: '/upload',
        method: 'post',
        auth: true,
        data: form,
      };
      axiosRequest(option).then(res => {
        let urlImage = `${API_ROOT}/${res.url}`;
        props.changeUrl(urlImage);
      });
    },
  });
  const removePreview = function() {
    props.changeUrl('');
    setFiles([]);
  };
  const thumbs = files.map(file => (
    <div className="thumbImage" key={file.name} id="preview_image">
      <div className="thumbInner">
        <a
          onClick={removePreview}
          className="removePreview"
          href="javascript:;"
        >
          <HighlightOffIcon />
        </a>
        <img src={file.preview} className="imgPreview" />
      </div>
    </div>
  ));

  const thumbDefault = (files) => {
    return files.map( (file , key) => {
    
      return (
        file != '' ?
        <div className="thumbImage" key={key}>
          <div className="thumbInner">
            <a
              onClick={removePreview}
              className="removePreview"
              href="javascript:;"
            >
              <HighlightOffIcon />
            </a>
            <img src={file} className="imgPreview" />
        </div>
      </div> : null
      )
    })
  }

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [files],
  );

  const rendePreview = () => {
    const {type} = props;
      if(type != 'add'){
        return (files.length == 0) ? thumbDefault(props.thumbDefault) : thumbs
      }
      else {
        return thumbs;
      }
  } 
  return (
    <section className="dropzoneImage">
      <div
        {...getRootProps({
          className: dragEnter
            ? 'dropzone dropable active'
            : 'dropzone dropable',
        })}
      >
        <input {...getInputProps()} />
        <div className="coverMess">
          <PhotoCameraIcon fontSize="large" color="primary" />
          <p> Drag and drop or select image files (accept png, jpg) 2mb</p>
        </div>
      </div>
      <aside className="thumbsContainer">  { rendePreview()}  </aside>
    </section>
  );
}
