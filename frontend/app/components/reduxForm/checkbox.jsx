import React, { Component } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

export default class renderFieldCheckbox extends Component {
  render() {
    const {
      input,
      label,
      meta: { touched, error },
    } = this.props;

    if (touched && error) {
      alert(error);
    }
    return (
      <FormControlLabel
        style={{ marginBottom: 0, marginRight: '5px' }}
        control={
          <Checkbox
            onChange={() => input.onChange(input.value)}
            name={`${input.name}`}
          />
        }
        label={label}
      />
    );
  }
}
