import React from 'react';

import { TextField } from '@material-ui/core';
const renderField = ({
  input,
  placeholder,
  type,
  autoFocus,
  maxLength,
  disable,
  label,
  meta: { touched, error },
}) => (
  <div className={'input-render'} name={`position-${input.name}`}>
    <TextField
      fullWidth
      label={label}
      type={type}
      autoFocus={autoFocus}
      maxLength={maxLength}
      disable={disable}
      placeholder={placeholder}
      margin="normal"
      name={input.name}
      variant="outlined"
      size={'small'}
      multiline
      rows={5}
      rowsMax={8}
      {...input}
    />

    {touched && error && <span className="text-danger">{error}</span>}
  </div>
);

export default renderField;
