import React, { useImperativeHandle, forwardRef, useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';
import Draggable from 'react-draggable';

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const DraggableDialog = forwardRef((props, ref) => {
  const [open, setOpen] = useState(false);
  const [idHandel, setIdHandel] = useState('');

  const handleClose = () => {
    setOpen(false);
  };

  const handleFunc = () => {
    let status = props.onClickOk({
      id: idHandel,
      url: `/manufacturers/${idHandel}`,
      close: handleClose,
    });
  };

  useImperativeHandle(ref, () => ({
    handleClickOpen(id) {
      setOpen(true);
      setIdHandel(id);
    },
  }));

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          {props.title}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>{props.body}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleFunc} className="btn btn-dagger">
            DELETE
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});
export default DraggableDialog;
